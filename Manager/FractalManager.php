<?php

namespace Soluti\DataFilterBundle\Manager;

use League\Fractal\Manager;
use Symfony\Component\HttpFoundation\RequestStack;

class FractalManager
{
    /** @var Manager $fractal */
    protected $fractal;
    /** @var RequestStack $requestStack */
    protected $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->fractal = new Manager();
        $this->requestStack = $requestStack;
    }

    public function createData($resource)
    {
        $this->parseQueryParams();

        return $this->fractal->createData($resource);
    }

    private function parseQueryParams()
    {
        $this->fractal->parseIncludes(
            $this->requestStack->getCurrentRequest()->get('include', '')
        );
        $this->fractal->parseExcludes(
            $this->requestStack->getCurrentRequest()->get('exclude', '')
        );
    }
}
