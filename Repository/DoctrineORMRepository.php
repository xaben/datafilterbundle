<?php

namespace Soluti\DataFilterBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Soluti\DataFilterBundle\Definition\DoctrineORMFilterDefinitionInterface;
use Soluti\DataFilterBundle\Filter\CollectionFilter;
use Soluti\DataFilterBundle\Filter\FilterResult;
use Traversable;

abstract class DoctrineORMRepository extends EntityRepository implements FilterableRepositoryInterface
{
    /**
     * @param CollectionFilter $filter
     *
     * @return FilterResult
     */
    public function findFiltered(CollectionFilter $filter)
    {
        return new FilterResult(
            $filter,
            $this->getTotalCount($filter),
            $this->getFilteredCount($filter),
            $this->getData($filter)
        );
    }

    /**
     * @param CollectionFilter $filter
     * @return Traversable
     */
    protected function getData(CollectionFilter $filter)
    {
        $qb = $this->getFilterQueryBuilder($filter);

        // sorting
        foreach ($filter->getSortOrder() as $sortKey => $sortOrder) {
            $qb->addOrderBy($sortKey, $sortOrder);
        }

        // filtering
        foreach ($filter->getCriteria() as $criteria) {
            $qb->andWhere($criteria['statement']);
            if (isset($criteria['parameter'])) {
                $qb->setParameter($criteria['parameter']['key'], $criteria['parameter']['value']);
            }
        }

        // pagination
        $qb->setFirstResult($filter->getOffset());
        $qb->setMaxResults($filter->getLimit());

        return new ArrayCollection($qb->getQuery()->getResult());
    }

    /**
     * @param CollectionFilter $filter
     * @return int
     */
    public function getFilteredCount(CollectionFilter $filter)
    {
        return $this->getCount(
            $this->getFilterQueryBuilder($filter),
            $filter->getCriteria()
        );
    }

    /**
     * @param CollectionFilter $filter
     * @return int
     */
    public function getTotalCount(CollectionFilter $filter)
    {
        return $this->getCount(
            $this->getFilterQueryBuilder($filter),
            $filter->getPredefinedCriteria()
        );
    }

    /**
     * @param QueryBuilder $qb
     * @param array $criteria
     * @return int
     */
    protected function getCount(QueryBuilder $qb, array $criteria)
    {
        foreach ($criteria as $filter) {
            $qb->andWhere($filter['statement']);
            if (isset($filter['parameter'])) {
                $qb->setParameter($filter['parameter']['key'], $filter['parameter']['value']);
            }
        }

        $qb->select('COUNT(1)');

        return (int)$qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param CollectionFilter $filter
     * @return QueryBuilder
     */
    private function getFilterQueryBuilder(CollectionFilter $filter)
    {
        $filterDefinition = $filter->getDefinition();

        if ($filterDefinition instanceof DoctrineORMFilterDefinitionInterface) {
            return $filterDefinition->getQueryBuilder($this);
        }

        return $this
            ->createQueryBuilder('object')
            ->select('object')
            ->where('1 = 1');
    }
}
