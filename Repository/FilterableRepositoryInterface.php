<?php

namespace Soluti\DataFilterBundle\Repository;

use Soluti\DataFilterBundle\Filter\CollectionFilter;
use Soluti\DataFilterBundle\Filter\FilterResult;

interface FilterableRepositoryInterface
{
    /**
     * @param CollectionFilter $filter
     *
     * @return FilterResult
     */
    public function findFiltered(CollectionFilter $filter);
}
