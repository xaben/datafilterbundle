<?php

namespace Soluti\DataFilterBundle\Repository;

use MongoDB\Driver\Cursor;
use Soluti\DataFilterBundle\Filter\CollectionFilter;
use Soluti\DataFilterBundle\Filter\FilterResult;
use Soluti\MongoBundle\Repository\BaseRepository;

abstract class MongoRepository extends BaseRepository implements FilterableRepositoryInterface
{
    /**
     * @param CollectionFilter $filter
     *
     * @return FilterResult
     */
    public function findFiltered(CollectionFilter $filter)
    {
        $criteria = $this->prepareCriteria($filter->getCriteria());
        $predefinedCriteria = $this->prepareCriteria($filter->getPredefinedCriteria());

        $options = [];

        // Pagination
        if ($filter->getLimit()) {
            $options['limit'] = $filter->getLimit();
        }

        if ($filter->getOffset()) {
            $options['skip'] = $filter->getOffset();
        }

        // Sorting
        if ($filter->getSortOrder()) {
            $options['sort'] = array_map(
                function ($value) {
                    return $value == 'asc' ? 1 : -1;
                },
                $filter->getSortOrder()
            );
        }

        /** @var Cursor $cursor */
        $cursor = $this->getCollection()->find($criteria, $options);

        return new FilterResult(
            $filter,
            $this->getCollection()->count($predefinedCriteria),
            $this->getCollection()->count($criteria),
            $this->collection($cursor)
        );
    }
}
