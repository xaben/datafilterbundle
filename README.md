Soluti DataFilterBundle
=======================

A bundle useful for filtering data coming from different sources(adapters) and formatting them back into the expected format.

Supports MongoDB and Doctrine ORM.

## Installation

Require the `soluti/data-filter-bundle` package in your composer.json and update your dependencies.

    $ composer require soluti/data-filter-bundle

Add the SolutiDataFilterBundle to your application's kernel:

```php
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new Soluti\DataFilterBundle\SolutiDataFilterBundle(),
            // ...
        );
        // ...
    }
```

## Configuration

```yaml
soluti_data_filter:
    db_driver: doctrine_orm
```

TODO: Tests, documentation and installation examples.
