<?php

namespace Soluti\DataFilterBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class SolutiDataFilterExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $this->configureAdapters($container, $config);
    }

    /**
     * @param ContainerBuilder $container
     * @param $config
     * @throws \Exception
     */
    private function configureAdapters(ContainerBuilder $container, $config)
    {
        switch ($config['db_driver']) {
            case 'doctrine_orm':
                $dbDriver = new Reference('soluti_data_filter.db.doctrine');
                break;
            case 'mongodb':
                $dbDriver = new Reference('soluti_data_filter.db.mongo');
                break;
            default:
                $dbDriver = new Reference($config['db_driver']);
        }

        $container
            ->getDefinition('soluti_data_filter.adapter.api')
            ->replaceArgument(0, $dbDriver);

        $container
            ->getDefinition('soluti_data_filter.adapter.data_table')
            ->replaceArgument(0, $dbDriver);
    }
}
