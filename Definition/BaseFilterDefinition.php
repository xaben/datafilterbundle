<?php

namespace Soluti\DataFilterBundle\Definition;

use Soluti\DataFilterBundle\Repository\FilterableRepositoryInterface;
use Soluti\DataFilterBundle\Transformer\TransformerInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class BaseFilterDefinition implements FilterDefinitionInterface
{
    /** @var FilterableRepositoryInterface */
    private $repository;

    /** @var TransformerInterface */
    private $transformer;

    /**
     * @param FilterableRepositoryInterface $repository
     * @param TransformerInterface $transformer
     */
    public function __construct(FilterableRepositoryInterface $repository, TransformerInterface $transformer)
    {
        $this->repository = $repository;
        $this->transformer = $transformer;
    }

    public function isPaginated(): bool
    {
        return true;
    }

    public function getSortableFields(): array
    {
        return [];
    }

    public function getColumns(): array
    {
        return [];
    }

    public function getFilterConfiguration(): array
    {
        return [];
    }

    public function getDefaultFilters(Request $request): array
    {
        return [];
    }

    public function getDefaultSort(): array
    {
        return [];
    }

    public function getPredefinedFilters(Request $request): array
    {
        return [];
    }

    public function getRepositoryService(): FilterableRepositoryInterface
    {
        return $this->repository;
    }

    public function getTransformerService(): TransformerInterface
    {
        return $this->transformer;
    }
}
