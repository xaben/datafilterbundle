<?php

namespace Soluti\DataFilterBundle\Definition;

use Soluti\DataFilterBundle\Repository\FilterableRepositoryInterface;
use Soluti\DataFilterBundle\Transformer\TransformerInterface;
use Symfony\Component\HttpFoundation\Request;

interface FilterDefinitionInterface
{
    public function isPaginated(): bool;

    public function getColumns(): array;

    public function getFilterConfiguration(): array;

    public function getDefaultFilters(Request $request): array;

    public function getPredefinedFilters(Request $request): array;

    public function getSortableFields(): array;

    public function getDefaultSort(): array;

    public function getRepositoryService(): FilterableRepositoryInterface;

    public function getTransformerService(): TransformerInterface;
}
