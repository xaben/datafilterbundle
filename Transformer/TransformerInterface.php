<?php

namespace Soluti\DataFilterBundle\Transformer;

use Traversable;

interface TransformerInterface
{
    /**
     * @param \Traversable $data
     *
     * @return array
     */
    public function transformCollection(Traversable $data);

    /**
     * @param mixed $data
     *
     * @return array
     */
    public function transform($data);
}
