<?php

namespace Soluti\DataFilterBundle\Transformer;

use Traversable;

abstract class AbstractTransformer implements TransformerInterface
{
    public function transformCollection(Traversable $data)
    {
        $result = [];
        foreach ($data as $item) {
            $result[] = $this->transform($item);
        }

        return $result;
    }

    /**
     * @param mixed $data
     * @return array
     */
    abstract public function transform($data);
}
