<?php

namespace Soluti\DataFilterBundle\Transformer;

use AppBundle\Manager\FractalManager;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\ResourceAbstract;
use League\Fractal\TransformerAbstract;
use Traversable;

abstract class ApiAbstractTransformer extends TransformerAbstract implements TransformerInterface
{
    /** @var FractalManager */
    protected $fractalManager;

    /**
     * @param FractalManager $fractalManager
     */
    public function __construct(FractalManager $fractalManager)
    {
        $this->fractalManager = $fractalManager;
    }

    protected function fractalResourceToArray(ResourceAbstract $resource)
    {
        return $this->fractalManager->createData($resource)->toArray();
    }

    public function transformCollection(Traversable $data)
    {
        return $this->fractalResourceToArray(new FractalCollection($data, $this));
    }

    /**
     * @param mixed $data
     * @return array
     */
    abstract public function transform($data);
}
