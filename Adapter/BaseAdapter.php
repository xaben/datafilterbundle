<?php

namespace Soluti\DataFilterBundle\Adapter;

use Soluti\DataFilterBundle\Adapter\DB\DBInterface;
use Soluti\DataFilterBundle\Definition\FilterDefinitionInterface;
use Soluti\DataFilterBundle\Filter\CollectionFilter;
use Soluti\DataFilterBundle\Filter\FilterResult;
use Soluti\DataFilterBundle\Formatter\FormatterInterface;
use Soluti\DataFilterBundle\Model\SortDirection;
use Symfony\Component\HttpFoundation\Request;

abstract class BaseAdapter
{
    /**
     * @var FormatterInterface
     */
    protected $formatter;

    /** @var DBInterface */
    protected $db;

    /**
     * @param DBInterface $db
     * @param FormatterInterface $formatter
     */
    public function __construct(DBInterface $db, FormatterInterface $formatter)
    {
        $this->formatter = $formatter;
        $this->db = $db;
    }

    /**
     * Builds a Collection filter based on definition and Request
     *
     * @param string|FilterDefinitionInterface $definition
     * @param Request $request
     * @param CollectionFilter|null $collectionFilter
     *
     * @return array
     *
     * @throws \Exception
     */
    public function process($definition, Request $request, CollectionFilter $collectionFilter = null)
    {
        if (!$definition instanceof FilterDefinitionInterface) {
            if (!in_array(FilterDefinitionInterface::class, class_implements($definition))) {
                throw new \Exception(
                    sprintf('%s does not implement FilterDefinitionInterface interface.', $definition)
                );
            }

            /** @var FilterDefinitionInterface $definition */
            $definition = new $definition;
        }

        $repository = $definition->getRepositoryService();
        $transformer = $definition->getTransformerService();

        $filter = $this->getFilter($definition, $request, $collectionFilter);

        /** @var FilterResult $data */
        $data = $repository->findFiltered($filter);

        return $this->formatter->format($data, $transformer);
    }

    /**
     * @param FilterDefinitionInterface $definition
     * @param Request $request
     * @param CollectionFilter|null $collectionFilter
     *
     * @return CollectionFilter
     */
    protected function getFilter(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter = null
    ) {
        if (is_null($collectionFilter)) {
            $collectionFilter = new CollectionFilter($definition);
        }

        $this->processPagination($definition, $request, $collectionFilter);
        $this->processSortable($definition, $request, $collectionFilter);
        $this->processFilters($definition, $request, $collectionFilter);

        return $collectionFilter;
    }

    /**
     * Prepare query params for pagination
     *
     * @param FilterDefinitionInterface $definition
     * @param Request $request
     * @param CollectionFilter $collectionFilter
     *
     * @return mixed
     */
    abstract protected function processPagination(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    );

    /**
     * Prepare query params for sortable
     *
     * @param FilterDefinitionInterface $definition
     * @param Request $request
     * @param CollectionFilter $collectionFilter
     *
     * @return mixed
     */
    abstract protected function processSortable(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    );

    /**
     * Prepare query params for filters
     *
     * @param FilterDefinitionInterface $definition
     * @param Request $request
     * @param CollectionFilter $collectionFilter
     *
     * @return mixed
     */
    abstract protected function processFilters(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    );

    /**
     *
     * @param FilterDefinitionInterface $definition
     * @param $columnName
     * @param $value
     *
     * @return array
     */
    protected function getFieldFilter(FilterDefinitionInterface $definition, $columnName, $value)
    {
        $configuration = $definition->getFilterConfiguration()[$columnName];
        $value = $this->db->prepareValue($value, $configuration);

        if (array_key_exists('columnName', $configuration)) {
            $columnName = $configuration['columnName'];
        }

        return $this->db->prepareOperation($value, $columnName, $configuration);
    }

    /**
     * Checks if value is not null or empty string.
     * 0, false '0' are considered valid values.
     *
     * @param mixed $value
     * @return bool
     */
    protected function isEmpty($value): bool
    {
        if (is_array($value)) {
            $isEmpty = true;
            foreach ($value as $item) {
                $isEmpty = $isEmpty && $this->isEmpty($item);
            }

            return $isEmpty;
        }

        return is_null($value) || $value === '';
    }

    protected function validateDefaultSort($defaultSort)
    {
        $normalizedDefaultSort = [];
        foreach ($defaultSort as $sortKey => $sortDirection) {
            $normalizedDefaultSort[$sortKey] = $this->db->prepareSortDirection(new SortDirection($sortDirection));
        }

        return $normalizedDefaultSort;
    }
}
