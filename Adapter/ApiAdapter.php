<?php

namespace Soluti\DataFilterBundle\Adapter;

use Soluti\DataFilterBundle\Definition\FilterDefinitionInterface;
use Soluti\DataFilterBundle\Filter\CollectionFilter;
use Soluti\DataFilterBundle\Model\SortDirection;
use Symfony\Component\HttpFoundation\Request;

class ApiAdapter extends BaseAdapter implements AdapterInterface
{
    protected function processPagination(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    ) {
        if (!$definition->isPaginated()) {
            return;
        }

        $page = (int)$request->query->get('page', 1);
        $perPage = (int)$request->query->get('per_page', AdapterInterface::DEFAULT_RESULT_COUNT);

        if ($perPage > AdapterInterface::MAX_RESULT_COUNT) {
            $perPage = AdapterInterface::MAX_RESULT_COUNT;
        }

        if ($perPage < 1) {
            $perPage = AdapterInterface::DEFAULT_RESULT_COUNT;
        }

        if ($page < 1) {
            $page = 1;
        }

        $collectionFilter->setOffset(($page - 1) * $perPage);
        $collectionFilter->setLimit($perPage);
    }

    protected function processSortable(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    ) {
        foreach ($request->query->get('order', []) as $columnName => $sortOrder) {
            if (in_array($columnName, array_keys($definition->getSortableFields()))) {
                $realKey = $definition->getSortableFields()[$columnName];
                $sort[$realKey] = $this->db->prepareSortDirection(new SortDirection($sortOrder));
            }
        }

        $collectionFilter->setSortOrder($sort ?? $this->validateDefaultSort($definition->getDefaultSort()));
    }

    protected function processFilters(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    ) {
        $criteria = [];
        foreach ($request->query->get('filter', []) as $columnName => $value) {
            if (!array_key_exists($columnName, $definition->getFilterConfiguration()) || $this->isEmpty($value)) {
                continue;
            }

            // do not add filter if not in values list
            $configuration = $definition->getFilterConfiguration()[$columnName];
            $preparedValues = (array)$this->db->prepareValue($value, $configuration);
            if (array_key_exists('values', $configuration) &&
                count($preparedValues) !== count(array_intersect($preparedValues, $configuration['values']))
            ) {
                continue;
            }

            $fieldCriteria = $this->getFieldFilter($definition, $columnName, $value);
            $criteria = array_merge($criteria, $fieldCriteria);
        }

        $predefinedFilters = $definition->getPredefinedFilters($request);
        $collectionFilter->setCriteria(
            array_merge(
                $definition->getDefaultFilters($request),
                $criteria,
                $predefinedFilters
            )
        );

        $collectionFilter->setPredefinedCriteria($predefinedFilters);
    }
}
