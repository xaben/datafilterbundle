<?php

namespace Soluti\DataFilterBundle\Adapter\DB;

use DateTime;
use MongoDB\BSON\ObjectID;
use MongoDB\BSON\Regex;
use MongoDB\BSON\UTCDateTime;
use Soluti\DataFilterBundle\Model\SortDirection;

class Mongo implements DBInterface
{
    /**
     * @param mixed $value
     * @param array $configuration
     *
     * @return mixed
     */
    public function prepareValue($value, $configuration)
    {
        if (is_array($value)) {
            $new = [];
            foreach ($value as $key => $item) {
                $new[$key] = $this->prepareValue($item, $configuration);
            }

            return $new;
        }

        $value = trim($value);

        $result = null;
        switch ($configuration['type']) {
            case 'id':
                $result = new ObjectID($value);
                break;
            case 'string':
                $result = (string)$value;
                break;
            case 'float':
                $result = (float)$value;
                break;
            case 'int':
            case 'integer':
                $result = (int)$value;
                break;
            case 'bool':
            case 'boolean':
                $result = (bool)$value;
                break;
            case 'date':
                $parsedDate = DateTime::createFromFormat('d-m-Y', $value);
                if ($parsedDate) {
                    $parsedDate->setTime(0, 0, 0);
                    $result = new UTCDatetime($parsedDate->getTimestamp().'000');
                }
                break;
        }

        return $result;
    }

    /**
     * @param mixed $value
     * @param string $columnName
     * @param string $match
     *
     * @return array
     */
    public function prepareOperation($value, $columnName, $match)
    {
        $result = [];
        switch ($match) {
            case 'exact':
                $result = [$columnName => $value];
                break;
            case 'in':
                $result = [$columnName => ['$in' => (array)$value]];
                break;
            case 'start':
                $result = [
                    $columnName => new Regex(
                        '^'.preg_quote($value, '/'),
                        'i'
                    ),
                ];
                break;
            case 'range':
                $range = [];
                if (array_key_exists('start', $value) && !$this->isEmpty($value['start'])) {
                    $range['$gte'] = $value['start'];
                }

                if (array_key_exists('end', $value) && !$this->isEmpty($value['end'])) {
                    $range['$lte'] = $value['end'];
                }

                $result = [
                    $columnName => $range,
                ];

                break;
        }

        return $result;
    }

    /**
     * Checks if value is not null or empty string.
     * 0, false '0' are considered valid values.
     *
     * @param mixed $value
     * @return bool
     */
    private function isEmpty($value): bool
    {
        if (is_array($value)) {
            $isEmpty = true;
            foreach ($value as $item) {
                $isEmpty = $isEmpty && $this->isEmpty($item);
            }

            return $isEmpty;
        }

        return is_null($value) || $value === '';
    }

    public function prepareSortDirection(SortDirection $sortDirection)
    {
        return $sortDirection->getValue() === 'asc' ? 1 : -1;
    }
}
