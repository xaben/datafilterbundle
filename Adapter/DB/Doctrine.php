<?php

namespace Soluti\DataFilterBundle\Adapter\DB;

use Soluti\DataFilterBundle\Model\SortDirection;

class Doctrine implements DBInterface
{
    /**
     * @param mixed $value
     * @param array $configuration
     *
     * @return mixed
     */
    public function prepareValue($value, $configuration)
    {
        $value = trim($value);

        $result = null;
        switch ($configuration['type']) {
            case 'string':
                $result = (string)$value;
                break;
            case 'float':
                $result = (float)$value;
                break;
            case 'int':
            case 'integer':
                $result = (int)$value;
                break;
            case 'bool':
            case 'boolean':
                $result = (bool)$value;
                break;
            case 'array':
                $result = (array)$value;
                break;
            case 'callback':
                if (!is_callable($configuration['callback'])) {
                    throw new \UnexpectedValueException(
                        'The callback parameter should be a callable in the field configuration.'
                    );
                }

                $result = $configuration['callback']($value);
                break;
        }

        return $result;
    }

    /**
     * @param $value
     * @param $placeholder
     * @param $fieldDefinition
     *
     * @return array
     */
    public function prepareOperation($value, $placeholder, $fieldDefinition)
    {
        $result = [];
        switch ($fieldDefinition['match']) {
            case 'like':
                $result[0]['statement'] = $fieldDefinition['selector'].' LIKE :'.$placeholder;
                $result[0]['parameter']['key'] = $placeholder;
                $result[0]['parameter']['value'] = '%'.$value.'%';
                break;
            case 'exact':
                $result[0]['statement'] = $fieldDefinition['selector'].' = :'.$placeholder;
                $result[0]['parameter']['key'] = $placeholder;
                $result[0]['parameter']['value'] = $value;
                break;
            case 'in':
                $result[0]['statement'] = $fieldDefinition['selector'].' IN ( :'.$placeholder.' )';
                $result[0]['parameter']['key'] = $placeholder;
                $result[0]['parameter']['value'] = (array)$value;
                break;
            case 'instance':
                $result[0]['statement'] = $fieldDefinition['selector'].' INSTANCE OF '.$value;
                break;
        }

        return $result;
    }

    public function prepareSortDirection(SortDirection $sortDirection)
    {
        return $sortDirection->getValue();
    }
}
