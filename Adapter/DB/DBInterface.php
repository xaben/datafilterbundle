<?php

namespace Soluti\DataFilterBundle\Adapter\DB;

use Soluti\DataFilterBundle\Model\SortDirection;

interface DBInterface
{
    public function prepareValue($value, $configuration);

    public function prepareOperation($value, $placeholder, $fieldDefinition);

    public function prepareSortDirection(SortDirection $sortDirection);
}
