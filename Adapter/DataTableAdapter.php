<?php

namespace Soluti\DataFilterBundle\Adapter;

use Soluti\DataFilterBundle\Definition\FilterDefinitionInterface;
use Soluti\DataFilterBundle\Filter\CollectionFilter;
use Soluti\DataFilterBundle\Model\SortDirection;
use Symfony\Component\HttpFoundation\Request;

class DataTableAdapter extends BaseAdapter implements AdapterInterface
{
    protected function processPagination(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    ) {
        if (!$definition->isPaginated()) {
            return;
        }

        $offset = (int)$request->request->get('start', 0);
        $limit = (int)$request->request->get('length', AdapterInterface::DEFAULT_RESULT_COUNT);

        if ($limit < 0 || $limit > AdapterInterface::MAX_RESULT_COUNT) {
            $limit = AdapterInterface::MAX_RESULT_COUNT;
        }

        if ($offset < 0) {
            $offset = 0;
        }

        $collectionFilter->setOffset($offset);
        $collectionFilter->setLimit($limit);
    }

    protected function processSortable(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    ) {
        foreach ($request->request->get('order', []) as $sortOrder) {
            if (!array_key_exists($sortOrder['column'], $definition->getColumns())) {
                continue;
            }
            $columnName = $definition->getColumns()[$sortOrder['column']];
            if (in_array($columnName, array_keys($definition->getSortableFields()))) {
                $realKey = $definition->getSortableFields()[$columnName];
                $sort[$realKey] = $this->db->prepareSortDirection(new SortDirection($sortOrder['dir']));
            }
        }

        $collectionFilter->setSortOrder($sort ?? $this->validateDefaultSort($definition->getDefaultSort()));
    }

    protected function processFilters(
        FilterDefinitionInterface $definition,
        Request $request,
        CollectionFilter $collectionFilter
    ) {
        $criteria = [];
        foreach ($request->request->get('filter', []) as $key => $filter) {
            if (!array_key_exists('value', $filter) || $this->isEmpty($filter['value'])) {
                continue;
            }

            $value = $filter['value'];
            $columnName = $definition->getColumns()[$key];

            $fieldCriteria = $this->getFieldFilter($definition, $columnName, $value);
            $criteria = array_merge($criteria, $fieldCriteria);
        }

        $predefinedFilters = $definition->getPredefinedFilters($request);
        $collectionFilter->setCriteria(
            array_merge(
                $definition->getDefaultFilters($request),
                $criteria,
                $predefinedFilters
            )
        );

        $collectionFilter->setPredefinedCriteria($predefinedFilters);
    }
}
