<?php

namespace Soluti\DataFilterBundle\Model;

class SortDirection
{
    const SORT_ASC = 'asc';
    const SORT_DESC = 'desc';

    protected $normalizedValue;

    /**
     * @param int|string $value
     *
     * @throws \Exception
     */
    public function __construct($value)
    {
        if (is_string($value)) {
            $this->parseString($value);

            return;
        }

        if (is_integer($value)) {
            $this->parseInteger($value);

            return;
        }

        throw new \Exception('Unsupported sort direction.');
    }

    public function getValue()
    {
        return $this->normalizedValue;
    }

    private function parseInteger(int $value)
    {
        if (!in_array($value, [-1, 1])) {
            throw new \Exception('Unsupported sort direction.');
        }

        $this->normalizedValue = $value === 1 ? self::SORT_ASC : self::SORT_DESC;
    }

    private function parseString(string $value)
    {
        $value = strtolower($value);
        if (!in_array($value, [self::SORT_ASC, self::SORT_DESC])) {
            throw new \Exception('Unsupported sort direction.');
        }

        $this->normalizedValue = $value;
    }
}
