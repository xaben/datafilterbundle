<?php

namespace Soluti\DataFilterBundle\Formatter;

use Soluti\DataFilterBundle\Filter\FilterResult;
use Soluti\DataFilterBundle\Transformer\TransformerInterface;

class RawFormatter implements FormatterInterface
{
    /**
     * @param FilterResult $result
     * @param TransformerInterface $transformer
     * @return array
     */
    public function format(FilterResult $result, TransformerInterface $transformer)
    {
        return [
            'recordsTotal' => $result->getTotalResults(),
            'recordsFiltered' => $result->getFilteredResults(),
            'hasMore' => $result->hasMore(),
            'data' => $result->getData(),
        ];
    }
}
